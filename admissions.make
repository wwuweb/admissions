; Core version
; ------------

core = 7.x

; API version
; ------------

api = 2

; Core project
; ------------

projects[drupal][type] = core

; Collegesites profile
; --------------------

projects[collegesites][download][type] = "git"
projects[collegesites][download][tag] = "7.x-12.0.0"
projects[collegesites][download][url] = "https://bitbucket.org/wwuweb/collegesites.git"
projects[collegesites][type] = "module"

; Modules
; -------

projects[classy_paragraphs][version] = 1.0
projects[classy_paragraphs][type] = "module"

projects[date_repeat_entity][version] = 2.0
projects[date_repeat_entity][type] = "module"

projects[entity_background][version] = 1.0-alpha6
projects[entity_background][type] = "module"

projects[google_tag][version] = 1.4
projects[google_tag][type] = "module"

projects[paragraphs][version] = 1.0-rc5
projects[paragraphs][type] = "module"

projects[paragraphs_id][version] = 1.0-alpha2
projects[paragraphs_id][type] = "module"

projects[replicate][version] = 1.2
projects[replicate][type] = "module"

projects[uuid][version] = 1.3
projects[uuid][type] = "module"

projects[webform_views_select][version] = 1.0
projects[webform_views_select][type] = "module"

projects[wwu_unset_mail_sender][download][type] = "git"
projects[wwu_unset_mail_sender][download][tag] = "7.x-1.0"
projects[wwu_unset_mail_sender][download][url] = "https://bitbucket.org/wwuweb/wwu_unset_mail_sender.git"
projects[wwu_unset_mail_sender][type] = "module"

; Libraries
; ---------

libraries[parallax][download][type] = "get"
libraries[parallax][download][url] = "https://github.com/pixelcog/parallax.js/archive/v1.5.0.zip"
libraries[parallax][directory_name] = "parallaxjs"
libraries[parallax][destination] = "libraries"
